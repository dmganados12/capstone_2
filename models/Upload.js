const mongoose = require("mongoose");

const uploadSchema = new mongoose.Schema({    
    productImg: String, 
    cloudinary_id: String
});

module.exports = mongoose.model("Upload", uploadSchema);
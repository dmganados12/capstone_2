// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, 'Please indicate the Product Name']
	},
	description: {
		type: String,
		required: [true, 'Description is Required']
	},	
	sellingPrice: {
		type: Number,
		required: [true, 'Selling Price is Required']
	},
	stock : {
		type: Number,
		required: true
	},
	isActive: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	productImg: {
		type: String,
		required: false
	},
	cloudinary_id: String,
	ordered: [
		{
			userId: {
				type: String				
			},
			// customerName: {
			// 	type: String
			// },
			quantity: {
				type: Number
			},
			subtotal: {
				type: Number
			}
		}
	]
	
});

// [SECTION] Model
module.exports = mongoose.model("Product", productSchema);
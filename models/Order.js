// [SECTION] Dependecies and Modules
const mongoose = require('mongoose');


// [SECTION] Schema
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is Required']
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, 'The Product ID is required']
			},
			productImg: {
				type: String,
				required: true
			},
			cloudinary_id: String,
			name: {
				type: String,
				required: true
			},
			price: {
				type: Number,
				required: false
			},	
			quantity: {
				type: Number,
				required: true			
			},
			orderDate: {
				type: Date,
				default: new Date(),
			},
			subtotal: {
				type: Number,
				default: false
			}	
		}
	]
});

// [SECTION] Model
module.exports = mongoose.model("Order", orderSchema);

// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	phone: {
		type: Number,
		required: true
	},
	password: {
		type: String,
		required:[true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},		
	orders: [
		{
			productId: {
				type: String							
			},
			productImg: {
				type: String				
			},
			productName: {
				type: String
			},
			sellingPrice: {
				type: Number
			},	
			quantity: {
				type: Number							
			},
			subtotal: {
				type: Number
			}			
		}		
	]	
});

// [SECTION] Model
module.exports = mongoose.model('User', userSchema);
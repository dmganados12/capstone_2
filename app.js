// [SECTION] Dependencies and Modules
	const express = require('express');	
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');

// [SECTION] Environment Variable Setup
	dotenv.config();
	const port = process.env.PORT;
	const credential = process.env.MONGO_URL;

// [SECTION] Server Setup	
	const app = express();	
	app.use(cors());
	app.use(express.json());
	app.use(express.urlencoded({extended: false}));		
	app.use('/resources/uploads', express.static('resources/uploads'))

// [SECTION] Database Connect
	mongoose.connect(credential);
	const db = mongoose.connection;
	db.once('open', () => console.log (`Connected to Atlas`));

// [SECTION] Server Routes 
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);

// [SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send(`Project Already Deployed`)
	});
	app.get('/cors', (req, res) => {
		res.set('Access-Control-Allow-Origin', 'http://localhost:8000/');
		res.send({"msg": "CORS is enabled"})
	});
	app.listen(port, () => {
		console.log(`API is responding to port ${port}`);
	});


// [SECTION] Dependecies and Modules
const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// [SECTION] Functionalities - Create
module.exports.makeOrder = async (order) => {	
	// console.log(order)
	let mUser = order.userId;
	let mProduct = order.productId;
	let mQuantity = order.quantity;		
	let isCustomerOrdered = await Product.findById(mProduct).then(id => {	
		let image = id.productImg;		
		let name = id.productName;
		let price = id.sellingPrice;
		let getSubTotal = mQuantity * price;
		id.ordered.push({				
			userId: mUser,
			quantity: mQuantity,
			subtotal: getSubTotal
		});
		
		let data = {			
			productName: name,
			productImg: image,
			sellingPrice: price
		}
		return id.save().then((save, err) => {
			if (err) {
				return false
			} else {
				return data;
			}
		});		
	});		
	
	let isProductPurchased = await User.findById(mUser).then(user => {
		let itemPrice = isCustomerOrdered.sellingPrice;
		let orderQuantity = mQuantity;
		let getSubTotal = itemPrice * orderQuantity;
		user.orders.push({
			productId: mProduct,			
			productName: isCustomerOrdered.productName,
			productImg: isCustomerOrdered.productImg,
			sellingPrice: isCustomerOrdered.sellingPrice,
			quantity: mQuantity,
			subtotal: getSubTotal			
		});	
		
		return user.save().then((save, err) => {
			if (err) {
				return false;
			} else {
				return user;				
			}
		});		
	});		
		
	let productName = isCustomerOrdered.productName;
	let productImg = isCustomerOrdered.productImg;
	let itemPrice = isCustomerOrdered.sellingPrice;
	let subtotal = itemPrice * mQuantity
	let orderData = {
		productName: productName,
		productImg: productImg,
		sellingPrice: itemPrice,
		quantity: mQuantity,
		subtotal: subtotal
	}
	
	if (isCustomerOrdered && isProductPurchased) {
		return orderData;
	} else {
		return false;
	}
};

// [SECTION] Functionality [Retrieve]
module.exports.allOrders = (lists) => {
	return Product.find(lists).then(result => {
		let isActive = result.isActive;
		let orders = result.ordered;
		let name = result.productName;
		let data = {
			productName: name,
			isActive: isActive,
			orders: orders
		}
		return result;		
	});
};

module.exports.buyerOrder = (order) => {	
	return User.findById(order).then(buyer => {		
		let lastN = buyer.lastName;
		let ordered = buyer.orders;		
		let getTotal = buyer.orders.map(element => element.subtotal).reduce((prev, curr) => prev + curr, 0);
		let cart = {			
			totalAmount: getTotal,
			orders: ordered
		}
		return cart;		
	});	
};

// [SECTION] Functionality [Update]
module.exports.updateOrders = (order) => {		
	let orderId = order.orderId;	
	return User.findById(orderId).then(update => {		
		console.log(update);
	});	
};

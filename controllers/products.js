// [SECTION] 
	const Product = require('../models/Product');
	const Upload = require('../models/Upload');
	const upload = require('../upload');
	const dbConfig = require('../config/db');
	const url = dbConfig.url;	
	const cloudinary = require("cloudinary").v2;
	const multer = require("multer");
	const path = require("path")


// [SECTION] Functionalities - Create
	// Create products
	module.exports.createProducts = (item) => {		
		let iName = item.productName;
		let iDesc = item.description;
		let iPrice = item.sellingPrice;		
		let iStock = item.stock;
		let isActive = item.isActive;
		let image = item.productImg

		let newProduct = new Product({
			productName: iName,
			description: iDesc,			
			sellingPrice: iPrice,
			stock: iStock,
			isActive: isActive,
			productImg: image
		});
		return newProduct.save().then((saved, error) => {
			if (saved) {
				return saved
			} else {
				return 'Failed to create new product';
			}
		});		
	};

	// Upload Image
	module.exports.uploadFiles = (file) => {
		let fileUpload = file.productImg;
		let cloudinary = file.cloudinary_id;

		let newUpload = new Upload({
			productImg: fileUpload,
			// cloudinary_id: cloudinary
		})
		return newUpload.save().then((saved, error) => {
			if (saved) {
				return saved
			} else {
				return 'Failed to save image'
			}
		})
		// cloudinary.uploader.upload("./resources/uploads/newwhite.png", {
		// 	resources_type: "image"
		// }).then((result) => {
		// 	console.log("success", JSON.stringify(result, null, 2));
		// }).catch((error) => {
		// 	console.log("error", JSON.stringify(error, null, 2))
		// })
	};
	

// [SECTION] Functionalities - Retrieve
	// Retrieve all active products/listings
	module.exports.activeListings = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});
	};

	module.exports.allListings = () => {
		return Product.find({}).then(result => {
			return result
		});
	};

	// Retrieve single product
	module.exports.singleListing = (id) => {
		return Product.findById(id).then(outcome => {
			return outcome;
		});
	};

// [SECTION] Functionalities - Update
	// Update product info
	module.exports.updateProduct = (product, info) => {
		let iName = info.productName;
		let iDesc = info.description;
		let iPrice = info.sellingPrice;
		let iStock = info.stock;
		let isActive = info.isActive;
		let image = info.productImg
		let updatedProduct = {
			productName: iName,
			description: iDesc,
			sellingPrice: iPrice,
			stock: iStock,
			isActive: isActive,
			productImg: image
		};

		let itemId = product.productId;
		return Product.findByIdAndUpdate(itemId, updatedProduct).then((productUpdated, err) => {			
			if (productUpdated) {
				return(productUpdated)
			} else {
				return 'Failed to update the product'
			}
		});			
	};

	module.exports.updateNoImage = (product, info) => {
		let iName = info.productName;
		let iDesc = info.description;
		let iPrice = info.sellingPrice;
		let iStock = info.stock;
		let isActive = info.isActive;		
		let updated = {
			productName: iName,
			description: iDesc,
			sellingPrice: iPrice,
			stock: iStock,
			isActive: isActive			
		};

		let itemId = product.productId;
		return Product.findByIdAndUpdate(itemId, updated).then((productUpdated, err) => {			
			if (productUpdated) {
				return(productUpdated)
			} else {
				return 'Failed to update the product'
			}
		});			
	};

	// Archive product
	module.exports.archiveProduct = (product) => {
		let itemId = product.productId;
		let updates = {
			isActive: false
		}
		return Product.findByIdAndUpdate(itemId, updates).then((archived, err) => {
			if (archived) {
				return 'Product archived';
			} else {
				return 'Archiving unsuccessful';
			}
		});
	};

// [SECTION] Functionality [Delete]
	module.exports.deleteProduct = (listingId) => {
		return Product.findById(listingId).then(listing => 
		{
			if (listing === null) {
				return 'Listing not Found'
			} else {
				return listing.remove().then(
					(removedListing, err) => {
						if (err) {
							return 'Failed to delete Listing'
						} else {
							return 'Successfully destroyed listing'
						}
					}
				)
			}
		})
	}


// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Order = require('../models/Order');
	const Product = require('../models/Product');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Functionalities - Create

// Create New User
	module.exports.userRegistration = (reg) => {		
		let fName = reg.firstName;
		let lName = reg.lastName;
		let email = reg.email;
		let mobile = reg.phone;
		let passW = reg.password;
		let isAdmin = reg.isAdmin;		

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			phone: mobile,
			password: bcrypt.hashSync(passW, 10),
			isAdmin: isAdmin
		});
		return newUser.save().then((user, error) => {
			if (user) {
				return user;
			} else {
				return false
			}
		});
	}

	module.exports.checkEmail = (body) => {
		return User.find({email: body.email}).then(result => {
			if (result.length > 0) {
				return 'Email already exist'
			} else {
				return 'Email is still available'
			}
		});
	};

// Create Token
	module.exports.userToken = (reqBody) => {
		return User.findOne({email: reqBody.email}).then(outcome => {
			let passW = reqBody.password;
			if (outcome === null) {
				return false;
			}else {
				const isMatched = bcrypt.compareSync(passW, outcome.password);
				if (isMatched) {
					let userData = outcome.toObject();
					return {accessToken: auth.userAccessToken(userData)};
				} else {
					return false;
				}
			}

		})
	};

// [SECTION] Functionalities - Retrieve
// Retrieve All Users
	module.exports.getUsers = (id) => {
		return User.findById(id).then(outcome => {	
			let id = outcome._id;
			let fName = outcome.firstName;
			let lName = outcome.lastName;
			let email = outcome.email;
			let mobile = outcome.phone;
			let passW = outcome.password;
			let isAdmin = outcome.isAdmin;
			let userInfo = {
				_id: id,
				firstName: fName,
				lastName: lName,
				email: email,
				phone: mobile,
				password: passW,
				isAdmin: isAdmin
			}
			return userInfo;
		});
	};

// Retrieve Orders (Admin Only)
	module.exports.allOrders = () => {
		return User.find({}).then(result => {
			return result;
		});		
	};
	

// [SECTION] Functionalities - Update
	module.exports.asAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, error) => {
			if (admin) {
				return 'User is set as an admin';
			} else {
				return 'Changes failed'
			}
		});
	};

	module.exports.asNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return 'User is set as non-admin'
			} else {
				return 'Changes failed'
			};
		});
	};
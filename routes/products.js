// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');
	const multer = require('multer');	
	const mongoose = require('mongoose');
	const Product = require('../models/Product');
	const dotenv = require('dotenv')	
	const cloudinary = require('../config/db')
	const Upload = require('../models/Upload')
	const uploads = require('../upload')
	const DIR = process.env.CLOUDINARY_URL;	

// [SECTION] Routing Components
	const route = exp.Router();

// [SECTION] POST
	// Create Products (Admin Access Only)
	route.post('/', uploads.single("productImg"), auth.verify, async (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let name = await req.body.productName;
		let descrp = await req.body.description;
		let price = await req.body.sellingPrice;		
		let stck = await req.body.stock;
		let isActive = await req.body.isActive;
		let file = await cloudinary.uploader.upload(req.file.path);	
		let data = {
			productName: name,
			description: descrp,
			sellingPrice: price,
			isActive: isActive,
			stock: stck,
			productImg: file.secure_url,
			cloudinary_id: file.public_id
		}	
		if (isAdmin) {
			controller.createProducts(data).then(result => {
				res.send(result)
			})
		} else {
			res.send("Error")
		}
	})
	
	// Upload files/images
	route.post('/upload', uploads.single("productImg"), async (req, res) => {
		let file = await cloudinary.uploader.upload(req.file.path);
		let fileData = {
			productImg: file.secure_url,
			cloudinary_id: file.public_id
		}
		controller.uploadFiles(fileData).then(result => {
			res.send(result)
		})
		
	})

// [SECTION] GET
	// Active Products
	route.get('/all-active',(req, res) => {
		controller.activeListings().then(result => {
			res.send(result)
		});
	});

	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin
		isAdmin ? controller.allListings().then(result => res.send(result))
		: res.send('Unauthorized user');
	});

	// Single Product
	route.get('/:productId', (req, res) => {
		let data = req.params.productId;
		controller.singleListing(data).then(result => {
			res.send(result)
		});
	});

// [SECTION] PUT
	// Update Product
	route.put('/:productId/update-product', uploads.single("productImg"), auth.verify, async (req, res) => {
		let params = await req.params;
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let name = await req.body.productName;
		let descrp = await req.body.description;
		let price = await req.body.sellingPrice;		
		let stck = await req.body.stock;
		let isActive = await req.body.isActive;		
		let image = await cloudinary.uploader.upload(req.file.path);				
		let data = {
			productName: name,
			description: descrp,
			sellingPrice: price,
			isActive: isActive,
			stock: stck,			
			productImg: image.secure_url,
			cloudinary_id: image.public_id			
		}	
		{
			isAdmin?
			controller.updateProduct(params, data).then(result => {
				res.send(result)
			})
			:
				res.send('Error')
		}		
			

	});

	route.put('/:productId/update-no-image', auth.verify, (req, res) => {
		let params = req.params;
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let name = req.body.productName;
		let descrp = req.body.description;
		let price = req.body.sellingPrice;		
		let stck = req.body.stock;
		let isActive = req.body.isActive;		
		let data = {
			productName: name,
			description: descrp,
			sellingPrice: price,
			isActive: isActive,
			stock: stck					
		}
		{
			isAdmin ?
			controller.updateNoImage(params, data).then(result => {
				res.send(result)
			})
			:
			res.send('Error')
		}			
				
	});

	// Archive Product
	route.put('/:productId/archive', auth.verify,(req, res) => {
		let params = req.params;
		if (auth.decode(req.headers.authorization).isAdmin) {
			controller.archiveProduct(params).then(result => {
				res.send(result)
			})
		} else {
			res.send('User unauthorized')
		}
		
	});

// [SECTION] Delete
route.delete('/:productId/delete', auth.verify,(req, res) => {
	let token = req.headers.authorization;
	let isAdmin = auth.decode(token).isAdmin;
	let id = req.params.productId;
	
	isAdmin ? controller.deleteProduct(id).then(outcome => res.send(outcome))
	: res.send('Unauthorized user')
});

// [SECTION] Export Route System
module.exports = route;
// [SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controllers/orders');
const auth = require('../auth');

// [SECTION] Routing Component
const route = exp.Router();

// [SECTION] Create
route.post('/create-order', auth.verify,(req, res) => {	
	let token = req.headers.authorization;
	let payload =  auth.decode(token);
	let clientId = payload.id;
	let isAdmin = payload.isAdmin;
	let product = req.body.productId;
	let count = req.body.quantity;
	let order = {
		userId: clientId,
		productId: product,
		quantity: count
	};
	if (!isAdmin) {
		controller.makeOrder(order).then(result => {
			res.send(result)			
		});		
	} else {
		res.send('Purchase unsuccessful. Please check you credentials.')
	}
});

// [SECTION] Retrieve
route.get('/order-list', (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	isAdmin ? controller.allOrders().then(result => 
		res.send(result))
	: res.send('Unauthorized user')
	
});

route.get('/customer-orders', auth.verify, (req, res) => {
	let customerInfo = auth.decode(req.headers.authorization);
	let customerId = customerInfo.id;
	controller.buyerOrder(customerId).then(customer => {
		res.send(customer);
	});
});

// [SECTION] Update
route.put('/update-orders/:orderId', (req, res) => {
	let params = req.params.orderId;
	let body = req.body.quantity;
	controller.updateOrders(params).then(result => {
		res.send(result);
	});
});

// [SECTION] Route System
module.exports = route;